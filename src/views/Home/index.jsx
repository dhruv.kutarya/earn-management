import React from 'react'
import Footer from '../../Components/Footer'
import Hero from '../../Components/Hero'
import Navbar from '../../Components/Navbar'
import Roadmap2 from '../../Components/Roadmap2'

const Home = () => {
  return (
    <>
      <Navbar />
      <Hero />
      <Roadmap2 />
      <Footer />
    </>
  )
}

export default Home
