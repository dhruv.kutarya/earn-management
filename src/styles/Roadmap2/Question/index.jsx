import styled from "styled-components";

export const Wrapper = styled.div`
    display: flex;
    flex-direction: ${props => props.right ? 'row-reverse' : 'row'};
    align-items: center;
`
export const Year = styled.p`
    font-size: 71px;
    margin: 5px 0;

    @media (max-width: 750px) {
        font-size: 40px;
    }
`
export const Quarter = styled.p`
    font-size: 33px;
    color: rgba(21, 196, 198, 1);
    font-weight: 600;
    margin: 0;

    @media (max-width: 750px) {
     font-size:25px ;
    }
`
export const Text = styled.p`
    color: rgba(255, 255, 255, 0.39);

    @media (max-width: 750px) {
        font-size:14px ;
    }
`
export const DivFigure = styled.div`
    background: ${props => props.first ? 'white' : 'rgba(29, 51, 64, 0.45)'};
    max-width: 214px;
    width: 100% ;
    height: ${props => props.first ? '179px' : '133px'};
    backdrop-filter: blur(40px);
    border-radius: 32px;
    display: flex;
    justify-content: center;
    align-items: center;

    @media (max-width:750px) {
        max-width: 100px;
        width: 100%;
        height: 80px;
        border-radius: 20px;
    }
`

export const Circle = styled.div`
    border-radius: 50%;
    background: ${props => props.first ? 'linear-gradient(180deg, #15C4C6 0%, #0C79F4 100%)' : 'linear-gradient(180deg, #778282 0%, #2F3842 100%)'};
    border: 6px solid rgba(255, 255, 255, 0.3);
    max-width: 102px;
    width: 100%;
    min-height: 102px;

    @media (max-width:750px) {
        max-width: 40px;
        min-height: 40px;
    }
`

export const Div = styled.div`
    max-width: ${props => props.maxwidth ? props.maxwidth : '550px'};
    text-align: ${props => props.right ? 'right' : 'left'};
    margin: 0 25px;

    @media (max-width: 750px) {
        margin: 0;
        width: 100%;
        transform: ${props=>props.right?'translateX(-20px)':'translateX(20px)'};
    }
`
export const MainDiv = styled.div`
    transform: ${props => props.right ? 'translateX(107px)' : 'translateX(-107px)'};

    @media (max-width: 750px) {
        transform: ${props => props.right ? 'translateX(50px)' : 'translateX(-50px)'};
    }
`