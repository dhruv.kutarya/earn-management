import styled from "styled-components";
export const Wrapper=styled.div`
    display: flex;
    flex: 1;
    justify-content: center;
    @media (max-width:750px){
        margin-top: 16px;
    }
`
export const Div=styled.div``

export const Flex=styled.div`
    display: flex;
    align-items: center;
    margin-left: ${props=>props.marginLeft};
    margin-top: ${props=>props.marginTop};
    @media (max-width:750px){
        justify-content: center;
    }
`
export const FlexIcon=styled(Flex)`
    margin-top: 30px;
    @media (max-width:750px){
        margin-top: 16px;
    }
`
export const Img=styled.img`
    height: ${props=>props.height?'18px':''};
    margin-right: 16px;
`
export const P=styled.p`
    color: ${props=>props.color};
`