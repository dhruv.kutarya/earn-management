import React from 'react'
import { ContactDiv, DivRoad, WrapperRoad, Heading, Desc, Img, RoadmapstartDiv, RoadmapStartContainer, UpperBorderDiv } from '../../styles/Roadmap2'
import { MainDiv, Wrapper, Div, DivFigure, Year, Quarter, Text, Circle } from '../../styles/Roadmap2/Question'
import ContactUs from './ContactUs'
import img from '../../assets/images/roadmapstart.png'


const Roadmap2 = () => {
  return (
    <WrapperRoad>
      <RoadmapStartContainer>
        <UpperBorderDiv />
        <RoadmapstartDiv>
          <div>
            <Heading>Road Map</Heading>
            <Desc>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Desc>
            <Img src={img} />
          </div>
          <MainDiv>
            <Wrapper>
              <DivFigure first>
                <Circle first/>
              </DivFigure>
              <Div maxwidth={'300px'}>
                <Year>2022</Year>
                <Quarter>Q1</Quarter>
                <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id purus augue. Lorem ipsum dolor</Text>
              </Div>
            </Wrapper>
          </MainDiv>
        </RoadmapstartDiv>
      </RoadmapStartContainer>

      <DivRoad right>
        <MainDiv right>
          <Wrapper right>
            <DivFigure>
              <Circle />
            </DivFigure>
            <Div right>
              <Year>2022</Year>
              <Quarter>Q2</Quarter>
              <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id purus augue. Lorem ipsum dolor</Text>
            </Div>
          </Wrapper>
        </MainDiv>
      </DivRoad>

      <DivRoad>
        <MainDiv >
          <Wrapper>
            <DivFigure>
              <Circle />
            </DivFigure>
            <Div>
              <Year>2022</Year>
              <Quarter>Q3</Quarter>
              <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id purus augue. Lorem ipsum dolor</Text>
            </Div>
          </Wrapper>
        </MainDiv>
      </DivRoad>

      <DivRoad right>
        <MainDiv right>
          <Wrapper right>
            <DivFigure>
              <Circle />
            </DivFigure>
            <Div right>
              <Year>2022</Year>
              <Quarter>Q4</Quarter>
              <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id purus augue. Lorem ipsum dolor</Text>
            </Div>
          </Wrapper>
        </MainDiv>
      </DivRoad>

      <DivRoad>
        <MainDiv>
          <Wrapper>
            <DivFigure>
              <Circle />
            </DivFigure>
            <Div>
              <Year>2023</Year>
              <Quarter>Q1</Quarter>
              <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id purus augue. Lorem ipsum dolor</Text>
            </Div>
          </Wrapper>
        </MainDiv>
      </DivRoad>

      <DivRoad right>
        <MainDiv right>
          <Wrapper right>
            <DivFigure>
              <Circle />
            </DivFigure>
            <Div right>
              <Year>2023</Year>
              <Quarter>Q2</Quarter>
              <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id purus augue. Lorem ipsum dolor</Text>
            </Div>
          </Wrapper>
        </MainDiv>
      </DivRoad>

      <ContactDiv>
        <ContactUs />
      </ContactDiv>
    </WrapperRoad>
  )
}

export default Roadmap2
